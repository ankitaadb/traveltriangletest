import { createStackNavigator } from 'react-navigation';
import { HomeContainer } from '../../screens/home/containers'
import { HomeDetailsContainer } from '../../screens/homeDetails/containers'

export default createStackNavigator({
    home: {
        screen: HomeContainer
    },
    homeDetails: {
        screen: HomeDetailsContainer
    }
    
});