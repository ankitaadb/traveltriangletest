export async function api({ absoluteUrl, method, data }) {
    try {
      const response = await fetch(absoluteUrl, {
        method,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
      const responseJson = await response.json();
      if (!response.ok || response.status !== 200) {
        throw responseJson;
      }
      return responseJson;
    } catch (error) {
      // console.error(error);
      return Promise.reject(error);
    }
  }