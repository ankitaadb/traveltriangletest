import React, { Component } from 'react';
import {
    View,
    Image,
    Dimensions,
} from 'react-native';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
import Icon from 'react-native-vector-icons/FontAwesome';


const HomeDetails = ({ selectedImage, navigation }) => {
    let imagePath = ('https://farm' + selectedImage.farm + '.static.flickr.com/' + selectedImage.server + '/' + selectedImage.id + '_' + selectedImage.secret + '.jpg')
    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, flexWrap: 'wrap', padding: 2 }}>
                    <Image resizeMode="cover" source={{ uri: imagePath }} style={{ width: width, height: height }} />
                </View>
            </View>
        </View>
    )
}


export default HomeDetails