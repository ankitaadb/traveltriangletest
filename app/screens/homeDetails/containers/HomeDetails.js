import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { HomeDetailsComponent } from '../components'
import { connect } from 'react-redux'
import { getSelectedImage } from '../../../selectors';

class HomeDetails extends Component {
    static navigationOptions = {
        title: 'Image',
    };
    render() {
        const { selectedImage, navigation } = this.props
        return (
            <HomeDetailsComponent 
            selectedImage={selectedImage}
            navigation={navigation}/>

        )
    }
}

const mapStateToProps = state => ({
    selectedImage: getSelectedImage(state)
})

export default connect(mapStateToProps)(HomeDetails)