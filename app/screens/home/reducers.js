
import { SAVE_IMAGES, SAVE_SELECTED_IMAGE, RESET_IMAGES } from "./constants";

const initialState = {
    images: [],
    selectedImage: {}
}

export default subreducer = (state = initialState, action) => {
    switch (action.type) {
        case SAVE_IMAGES: {
            const { images } = action
            return {
                ...state,
                images: [...state.images, ...images ]
            }
        }
        case RESET_IMAGES: {
            const { images } = action
            return {
                ...state,
                images: []
            }
        }
        case SAVE_SELECTED_IMAGE: {
            const { selectedImage } = action
            return {
                ...state,
                selectedImage
            }
        }
        default:
            return state;
    }
};