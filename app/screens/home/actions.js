import { api } from '../../appConfig/API/api'
import { SAVE_IMAGES, SAVE_SELECTED_IMAGE, RESET_IMAGES } from './constants';


export const fetchImages = (keyword, pageNumber) => dispatch => {
  api({
    absoluteUrl: 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736&format=json&nojsoncallback=1&safe_search=1&per_page=20&text=' + keyword + '&page=' + pageNumber,
    method: 'GET'
  })
    .then(data => {
      if (data.photos.photo) {
        dispatch({
          type: SAVE_IMAGES,
          images: data.photos.photo
        });
      }
    })
    .catch(() => {
      dispatch({
        type: SAVE_IMAGES,
        images: []
      });
    });
};

export const saveSelectedImage = selectedImage => dispatch => {
  dispatch({
    type: SAVE_SELECTED_IMAGE,
    selectedImage
  });
}

export const resetImages = () => dispatch => {
  dispatch({
    type: RESET_IMAGES
  });
}