import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { HomeComponent } from '../components'
import { connect } from 'react-redux'
import { fetchImages, saveSelectedImage, resetImages } from '../actions'
import { getImages } from '../../../selectors';

class Home extends Component {
    static navigationOptions = {
        title: 'Home',
    };
    constructor(props) {
        super(props)

        this.state = {
            scrollObject: { height: 0 },
            pageNumber: 1,
            keyword: 'kittens'
        }
    }


    componentDidMount() {
        this.getImagesByKeyword()
    }

    getImagesByKeyword = () => {
        const { fetchImages, resetImages } = this.props
        resetImages()
        fetchImages(this.state.keyword, this.state.pageNumber)
    }

    handleChange = (key, value) => {
        if (value !== this.state[key] && value.trim().length > 0) {
            this.setState({ [key]: value })
        }
    }

    infiniteScroll = (scrollObject) => {
        if (scrollObject.contentOffset.y > this.state.scrollObject.height &&
            scrollObject.contentOffset.y > scrollObject.contentSize.height - 1400) {
            this.setState({
                scrollObject: {
                    height: scrollObject.contentOffset.y,
                },
                page: this.state.pageNumber + 1
            },
                () => this.props.fetchImages(this.state.keyword, this.state.pageNumber))
            return
        }
        this.setState({
            scrollObject: {
                height: scrollObject.contentOffset.y
            }
        })

    }

    setSelectedImage = (selectedImage) => {
        const { saveSelectedImage, navigation } = this.props
        saveSelectedImage(selectedImage)
        navigation.navigate({ routeName: 'homeDetails' })
    }

    render() {
        const { images } = this.props
        return (
            <HomeComponent images={images}
                setSelectedImage={this.setSelectedImage}
                handleChange={this.handleChange}
                keyword={this.state.keyword}
                getImagesByKeyword={this.getImagesByKeyword}
                infiniteScroll={this.infiniteScroll}
            />

        )
    }
}

const mapStateToProps = state => ({
    images: getImages(state)
})

const mapDispatchToProps = {
    fetchImages,
    saveSelectedImage,
    resetImages
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)